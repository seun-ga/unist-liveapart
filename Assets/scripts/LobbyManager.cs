using UnityEngine;

public class LobbyManager : MonoBehaviour {

    public GameObject Lobby1;
    public GameObject Lobby2;

    public void TurnOnLobby1(){
        Lobby1.gameObject.SetActive(true);
        Lobby2.gameObject.SetActive(false);
    }
    public void TurnOnLobby2(){
        Lobby1.gameObject.SetActive(false);
        Lobby2.gameObject.SetActive(true);
    }

}