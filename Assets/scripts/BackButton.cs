/* Copyright (c) 2020-present Evereal. All rights reserved. */

namespace Evereal.VRVideoPlayer
{
  public class BackButton : ButtonBase
  {
      public LobbyManager lobbyManager;
    protected override void OnClick()
    {
        lobbyManager.TurnOnLobby1();
    }
  }
}