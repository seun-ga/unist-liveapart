using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class CommentManager : MonoBehaviour {
 
public Image panel_Left;
public Image panel_right;
public GameObject background;

public Text comment;
public float delayDuration=5.0f;

public void _DelayForComment(){
    InitializeDelay();
   
    comment.gameObject.SetActive(true);
    if(DataManager.Instance.IsPrepareNextChap){
      comment.text=(DataManager.Instance.currentChapter+1)+"막";
    }else{
      comment.text=DataManager.Instance.GetCurrentComment();
      DataManager.Instance.SetNextVideo();
      panel_Left.gameObject.SetActive(true);
      background.SetActive(true);
      panel_right.gameObject.SetActive(true);
    }
   
    
    
    StartCoroutine("DelayForComment");
}

public IEnumerator DelayForComment(){
    Debug.Log("Delay Start");
      float count=0.0f;
      while(delayDuration>count){
        count+=0.1f;
        //Reduce panel bar for waiting  
        panel_Left.fillAmount-=1.0f/(delayDuration/0.1f);
        panel_right.fillAmount-=1.0f/(delayDuration/0.1f);

        yield return new WaitForSeconds(0.1f);
      }
      Debug.Log("Delay Done");
    
    InitializeDelay();
}

private void InitializeDelay(){
    panel_right.fillAmount=1.0f;
    panel_Left.fillAmount=1.0f;
     background.SetActive(false);
    comment.gameObject.SetActive(false);
    panel_Left.gameObject.SetActive(false);
    panel_right.gameObject.SetActive(false);
}

public void StopDelay(){
  StopCoroutine("DelayForComment");
  InitializeDelay();
}

}