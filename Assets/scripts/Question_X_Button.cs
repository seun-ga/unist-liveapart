/* Copyright (c) 2020-present Evereal. All rights reserved. */
using UnityEngine;
using System.Collections;
namespace Evereal.VRVideoPlayer
{
  public class Question_X_Button : ButtonBase
  {

      public QuestionButton questionButton;
    
    protected override void OnClick()
    {
        questionButton.TurnOffQuestion();
    }
   

  }
}