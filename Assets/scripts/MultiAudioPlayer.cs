using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class MultiAudioPlayer : MonoBehaviour {
    public List<AudioSource> audioList = new List<AudioSource>();
    private int rewindTime=10;
    private float currentTime=0.0f;
    public float audioVolume=1.0f;

    public void Initialize(){
         foreach(AudioSource audio in audioList){
             //prepare for every gameobject
           audio.Stop();
        }
    }

    public /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
    foreach(AudioSource audio in audioList){
            audio.volume=audioVolume;
        }
    }


    public void Play(){
        foreach(AudioSource audio in audioList){
            audio.Play();
        }
    }
    public void Pause(){
        foreach(AudioSource audio in audioList){
           audio.Pause();
       }
    }
    public void RewindTime(){
        foreach(AudioSource audio in audioList){
          
           if(audio.time>=rewindTime){
               Debug.Log("time: "+audio.time);
                audio.time-=rewindTime;
                Debug.Log("winded time: "+audio.time);
           }
            else audio.time=0.0f;
        }
    }
}