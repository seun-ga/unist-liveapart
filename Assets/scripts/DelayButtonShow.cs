using UnityEngine;

public class DelayButtonShow : MonoBehaviour {
    public GameObject MainButton;
    public GameObject CancelButton;

    public int DelayNum;


    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        MainButton.SetActive(false);
        CancelButton.SetActive(false);
        Invoke("ShowButtons",DelayNum);
    }
    public void ShowButtons(){

        MainButton.SetActive(true);
        CancelButton.SetActive(true);

    } 
}