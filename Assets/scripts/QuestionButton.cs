/* Copyright (c) 2020-present Evereal. All rights reserved. */
using UnityEngine;
using System.Collections;
namespace Evereal.VRVideoPlayer
{
  public class QuestionButton : ButtonBase
  {

      public GameObject QuestionUI;
      private bool isOpen;
    
    protected override void OnClick()
    {
      if(isOpen){
          TurnOffQuestion();
      }else{
         TurnOnQuestion();
      } 
    }
    public void TurnOffQuestion(){
         QuestionUI.SetActive(false);
          isOpen=false;

    }
     public void TurnOnQuestion(){
          QuestionUI.SetActive(true);
          isOpen=true;

    }


  }
}