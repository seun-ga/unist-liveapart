using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
public class DataManager : MonoBehaviour {
    
    public int currentChapter=0;
    public bool isVideoScene=false;
    
    public int currentVideoIndex=0;
    //currnet step can be changed. it is made for saving points
    public int currentStep=0;
     public delegate void OnNextChapter();
     public static event OnNextChapter onNextChapter;
    public delegate void OnNextVideo();
    public static event OnNextChapter onNextVideo;

    public ChapterList chapterList;
  
    public AudioManager audioManager;
   public bool IsPrepareNextChap=false;

    private static DataManager _instance;
   
    public static DataManager Instance
    {
        get {
          
            if(!_instance)
            {
                _instance = FindObjectOfType(typeof(DataManager)) as DataManager;

                if (_instance == null)
                    Debug.Log("no Singleton obj");
            }
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        // 인스턴스가 존재하는 경우 새로생기는 인스턴스를 삭제한다.
        else if (_instance != this)
        {
            Destroy(gameObject);
        }
        // 아래의 함수를 사용하여 씬이 전환되더라도 선언되었던 인스턴스가 파괴되지 않는다.
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
    // onNextChapter+=SetNextChapter;
     onNextVideo+=SetNextVideo;
    }

    public void SetNextChapter(){
        if(currentChapter<chapterList.chapterLists.Count){
            IsPrepareNextChap=true;
            currentChapter++;
            currentVideoIndex=0;
            audioManager.SetChapterAudio(chapterList.chapterLists[currentChapter].audioList);
            Debug.Log("set next chapter");

        }else{
            
            SceneManager.LoadScene("Credit");
           
        }
    }
    public void SetNextVideo(){
        if(currentVideoIndex<chapterList.chapterLists[currentChapter].videoList.Count){
           currentVideoIndex++;
           audioManager.SetCurrentIndex(currentVideoIndex);
           Debug.Log("currentVideoIndex: "+currentVideoIndex);
        }
    }
  
    public List<string> GetCurrentChapterVideo(){
        Debug.Log("Video - current chapter is "+ chapterList.chapterLists[currentChapter].chapterNum);
        return chapterList.chapterLists[currentChapter].videoList;
    }

    public void SetCurrentChapter(){
        audioManager.SetChapterAudio(chapterList.chapterLists[currentChapter].audioList);
    }

    public void SetCertainAudio(int videoIndex){
        audioManager.SetCurrentIndex(currentVideoIndex);
    }

    public void AudioPlay(){
        audioManager.Play();
    }

    public void AudioPause(){
        audioManager.Pause();
    }
    public void AudioRewindTime(){
        audioManager.RewindTime();
    }
    public void AudioSetVolume(float volume){
        audioManager.SetVolume(volume);
    }

public string GetCurrentComment(){
    if(currentVideoIndex>=chapterList.chapterLists[currentChapter].commentList.Count){
        Debug.Log("Error. Write proper amount of comments");
        return null;
    }
    Debug.Log("check for currentComment : currentChapter: "+currentChapter+" videoIndex: "+currentVideoIndex);
    return chapterList.chapterLists[currentChapter].commentList[currentVideoIndex];
}

    public void Quit(){
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying=false;
        #else
            Application.Quit();
        #endif
    }

    public void GoToCertainScene(string sceneName){
           SceneManager.LoadScene(sceneName);
    }
    

}