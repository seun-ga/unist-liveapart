using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class AudioManager : MonoBehaviour {
    private List<MultiAudioPlayer> multiAudioPlayers=new List<MultiAudioPlayer>();
    public int currentIndex=0;
    public float audioVolume=1.0f;
   

    public void SetChapterAudio(List<MultiAudioPlayer> multiAudioList){
        multiAudioPlayers=multiAudioList;
        foreach(MultiAudioPlayer multiAudioPlayer in multiAudioPlayers){
            multiAudioPlayer.Initialize();
        }
        Debug.Log("Set Audio");
        StopAll();
        
    }

    public void SetCurrentIndex(int currentVideoIndex){
        currentIndex=currentVideoIndex;
        StopAll();
        Debug.Log("setCurrentIndex: "+currentIndex);
    }

    public void SetVolume(float   newVolume){
        multiAudioPlayers[currentIndex].audioVolume=newVolume;
        audioVolume=newVolume;
    }


    public void Play(){
       
        Debug.Log("AudioManager is Playing "+currentIndex);
        multiAudioPlayers[currentIndex].Play();
       
    }

    public void Pause(){
        multiAudioPlayers[currentIndex].Pause();
    }
    public void RewindTime(){
        multiAudioPlayers[currentIndex].RewindTime();
    }
public void StopAll(){
    foreach(MultiAudioPlayer multiAudioPlayer in multiAudioPlayers){
        multiAudioPlayer.Initialize();
              
    }
      Debug.Log("AudioManager is Initialize");

}


}