/* Copyright (c) 2020-present Evereal. All rights reserved. */
using UnityEngine;
using System.Collections;
namespace Evereal.VRVideoPlayer
{
  public class MainButton : ButtonBase
  {
    protected override void OnClick()
    {
        DataManager.Instance.GoToCertainScene("Lobby");
    }
   

  }
}