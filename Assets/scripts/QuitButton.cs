/* Copyright (c) 2020-present Evereal. All rights reserved. */
using UnityEngine;
using System.Collections;
namespace Evereal.VRVideoPlayer
{
  public class QuitButton : ButtonBase
  {
    protected override void OnClick()
    {
       DataManager.Instance.Quit();
    }
  }
}