/* Copyright (c) 2020-present Evereal. All rights reserved. */

namespace Evereal.VRVideoPlayer
{
  public class StopDelayButton : ButtonBase
  {
  
    public Fade fade;
    protected override void OnClick()
    {
    
        fade.StopDelay();
    }
  }
}